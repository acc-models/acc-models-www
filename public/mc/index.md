---
template: overrides/main.html
---

<h1> Muon Collider Optics Repository </h1>

**Work in progress.**

This website will contain the official optics models for the Muon Collider study. The repositories are available on Gitlab, AFS and EOS and can be accessed in the way described below. 

!!! note "Locations of the repositories on Gitlab, AFS and EOS"
		1) The different repositories are accessible on Gitlab using the following link:

			https://gitlab.cern.ch/acc-models/acc-models-mc.git

		2) The different repositories are also accessible on AFS:

			/afs/cern.ch/eng/acc-models/mc/

		3) The different repositories are also accessible on EOS:

			/eos/project/a/acc-models/public/mc/