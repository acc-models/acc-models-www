---
template: overrides/main.html
---

<h1> Proton Synchrotron Optics Repository </h1>

This website contains the official optics models for the CERN Proton Synchrotron. For each scenario listed below 
(and the various configurations belonging to each scenario), MAD-X input scripts and
optics plots are available and can be either browsed or downloaded directly. Furthermore, the repository is available on Gitlab, AFS and EOS and can be accessed in the way described below. 

!!! note "Locations of the repository on Gitlab, AFS and EOS"
		1) A local copy of the repository on your computer can be obtained by cloning the Gitlab repository using the following syntax:

			git clone https://gitlab.cern.ch/acc-models/acc-models-ps.git

		2) The repository is also accessible on AFS, where only MAD-X scripts and strength files are available:

			/afs/cern.ch/eng/acc-models/ps/

		3) The repository is also accessible on EOS, where all interactive plots can be found in addition to the data available on AFS:

			/eos/project/a/acc-models/public/ps/

The old PS optics repository is still available at this <a href = "http://cern-accelerators-optics.web.cern.ch/cern-accelerators-optics/PSoptics/defaultPS.htm" target=_blank>location</a>.
