<h1> LHC ION &#120574;<sub>t</sub> - jump</h1>

In order to cross transition energy in the PS, a &#120574;<sub>t</sub> - jump is applied using two quadrupole families, i.e. the doublets and the triplets.

Fig. 1 shows the magnetic cycle and the moment of transition crossing and Fig. 2 shows the &#120574;<sub>t</sub> evolution around transition energy, based on MAD-X simulations.
These simulations use the quadrupole strengths from LSA as input and an example script is available on [SWAN](https://cern.ch/swanserver/cgi-bin/go?projurl=/eos/project/a/acc-models/public/ps/supplementary/transition_crossing/lhc_ion/gammatr_evolution_lhc_ions.ipynb){target=_blank}.

<object width="50%" height=330px  data="magnetic_cycle.html"></object> 
<p style="text-align: center;"><b>Figure 1:</b> Magnetic configuration for the LHC ION cycle. The dashed line indicates the moment of transition crossing.</p>

<object width="70%" height=380px  data="gamma_jump.html"></object> 
<p style="text-align: center;"><b>Figure 1:</b> &#120574;<sub>t</sub> - jump configuration for the LHC ION cycle.</p>