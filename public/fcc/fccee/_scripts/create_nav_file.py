import pandas as pd
import jinja2
import glob
import sys
sys.path.insert(0, "/eos/project/a/acc-models/public/ps/2021/_scripts/web/")
from webtools import webtools

repo_directory = 'public/' 

# import content of mkdocs navigation files
folders = sorted(glob.glob('/eos/project/a/acc-models/public/fcc/fccee/[!_]*/'), reverse=True) 

nav_files = pd.DataFrame(columns = ['content'], index = folders)

for folder in folders:
    with open(folder + 'nav.yml') as f:
        nav_files['content'].loc[folder] = f.read()

templateLoader = jinja2.FileSystemLoader( searchpath="/eos/project/a/acc-models/public/fcc/fccee/_scripts")
templateEnv = jinja2.Environment(loader=templateLoader )

tyml = templateEnv.get_template('nav.yml.template')

rdata = {'nav_files': nav_files}


webtools.renderfile(['/eos/project/a/acc-models/public/fcc/fccee'], 'nav.yml', tyml, rdata)
