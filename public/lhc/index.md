---
template: overrides/main.html
---

<h1> Large Hadron Collider Optics Repository </h1>

Due to its large size, the LHC optics repository is not rendered into the web site.

The optics file can be found in https://gitlab.cern.ch/acc-models/acc-models-lhc.


The LHC optics repository for Run 2 and HL-LHC optics is still reachable using  the following link:

<a href="http://cern.ch/lhcoptics">http://cern.ch/lhcoptics</a>.
