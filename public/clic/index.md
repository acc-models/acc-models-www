---
template: overrides/main.html
---

<h1> Compact Linear Collider Optics Repository </h1>

![CLIC logo](logo.png){: style="width:90%"}

This website contains the official optics models for the Compact Linear Collider. The repositories are available on Gitlab, AFS and EOS and can be accessed in the way described below. 

!!! note "Locations of the repositories on Gitlab, AFS and EOS"
		1) The different repositories are accessible on Gitlab using the following link:

			https://gitlab.cern.ch/acc-models/acc-models-clic.git

		2) The different repositories are also accessible on AFS:

			/afs/cern.ch/eng/acc-models/clic/

		3) The different repositories are also accessible on EOS:

			/eos/project/a/acc-models/public/clic/