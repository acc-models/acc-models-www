<h1> Description of the LEIR optics model </h1>

<h2> Layout of the lattice </h2>

The LEIR lattice consists of four straight sections (SSs) and four arcs. SS10 accomodates the injection equipment, SS20 the electron cooler, SS30 the extraction kickers and SS40 the RF cavities and the extraction septum.
Figure 1 shows the layout of the accelerator, including its injection and extraction lines.

![LEIR](LEIR.jpg){: style="width:70%"}
<p style="text-align: center;"><b>Figure 1:</b> LEIR layout (from drawing  <a href="https://edms5.cern.ch/cdd/plsql/c4w_guided.guided_drawing?cookie=2466900&p_equip_id=LJ___" target=_blank>LEILJ___0001</a>).</p>

The total circumference of the machine is 

$$
\mathcal{C}_{\rm LEIR} = 25 \, \pi = 78.5398 \, {\rm m}
$$

and the bending radius of the main dipoles is

$$
\rho_{\rm LEIR} = 4.17 \, {\rm m}.
$$

<h3> LEIR bending magnets </h3>

Each of the four main dipole magnets consists of six iron yokes, which provide the 90 degrees bending. Figure 2 shows the arrangement of the different yokes and the blue text describes their names in the MAD-X sequence. 
Note that there is a drift space between the first and second (and the fifth and sixth), as well as between the central blocks.

![LEIR BHN](LEIR_bending_magnet.jpg){: style="width:70%"}
<p style="text-align: center;"><b>Figure 2:</b> Schematic drawing of the LEIR main dipole magnet ER.BHN (from the old drawing E10-1106-0).</p>

Inside the dipole magnets various equipment is installed. To model this properly, the different bending blocks are artificially split up in the following way in MAD-X: 

- To insert the Pole Face Windings (PFWs) into the first and last block, the **BA1/2H** are split into **BA1/2HO** and **BA1/2HI**, each of them having the length L = 1.11684/2 m.
- The magnets **BI1/2P** are split into three parts, i.e. **BI1/2PO**, **BI1/2PM** and **BI1/2PI** to install pick-ups. The total length of each of the two magnets **BI1/2PO** is L = 2.13554 m and the lengths of the splitted elements are:
	- **BI1/2PO**: L = 2.13554-0.2620-1.4778 = 0.39574 m
	- **BI1/2PM**: L = 0.2600 m
	- **BI1/2PI**: L = 1.4778 m
- The magnet **BI1PI** in ER.BHN40 is further split into two, i.e. **BI1PII** (L = 1.300 m) and **BI1PIO** (L = 0.1778 m), to install the horizontal ionization profile monitor.

<h3> LEIR quadrupole magnets </h3>

The quadrupoles are often very close to other elements, which disturbs their fringe field. Hence the quadrupole length is reduced by 0.005 m in case a sextupole or a skew quadrupole is placed next to it.

![LEIR quadrupoles](LEIR_quads.jpg){: style="width:40%"}
<p style="text-align: center;"><b>Figure 3:</b> LEIR quadrupoles installed in SS10. The two elements just upstream of each quadrupole are sextupole magnets (from drawing <a href="https://edms5.cern.ch/cdd/plsql/c4w_guided.guided_drawing?cookie=2466900&p_equip_id=LJ___" target=_blank>LEILJ___0002</a>).</p>

Furthermore, the elements ER.QFN22 and ER.QFN41/42 are split into two to accommodate pick-ups.