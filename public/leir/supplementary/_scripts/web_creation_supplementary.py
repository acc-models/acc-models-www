import datetime
import tfs
import glob
import numpy as np
import jinja2
import pandas as pnd
import sys
sys.path.insert(0, "/eos/project/a/acc-models/public/ps/2021/_scripts/web/")
from webtools import webtools
from os.path import relpath
from bokeh.plotting import figure, output_file, output_notebook, show, save, ColumnDataSource
import matplotlib.pyplot as plt
from matplotlib.colors import rgb2hex

year = datetime.datetime.now().year

directory = '/eos/project/a/acc-models/public/leir/'

branch = '2021'

templateLoader = jinja2.FileSystemLoader( searchpath=directory + "supplementary/_scripts/templates/" )
templateEnv = jinja2.Environment(loader=templateLoader )
tnav = templateEnv.get_template('nav.yml.template')

# ---------------------------------------------------------------------
# Mkdocs navigation file
# ---------------------------------------------------------------------

rdata = {}

webtools.renderfile([directory + 'supplementary/'], 'nav.yml', tnav, rdata)
